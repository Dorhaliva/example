var express = require('express');
var app = express();
require('dotenv').config()

// Routes
app.get('/', function (req, res) {
  pool.query('SELECT * from account', (err, results) => {
    res.status(200).json(results.rows)
  })
});

// Listen
var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'

app.listen(server_port, server_ip_address, function () {
  console.log("Listening on " + server_ip_address + ", port " + server_port)
});


const { Pool, Client } = require('pg')

const pool = new Pool({
  user: process.env.POSTGRESQL_USER,
  host: process.env.POSTGRESQL_SERVICE_HOST,
  database: process.env.POSTGRESQL_DATABASE,
  password: process.env.POSTGRESQL_PASSWORD,
  port: process.env.POSTGRESQL_SERVICE_PORT_POSTGRESQL,
})


